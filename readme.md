# com.hcnx.version

[![CI Status](http://img.shields.io/travis/Guillaume MARTINEZ/com.hcnx.version.svg?style=flat)](https://travis-ci.org/Guillaume MARTINEZ/com.hcnx.version)
[![Version](https://img.shields.io/cocoapods/v/com.hcnx.version.svg?style=flat)](http://cocoapods.org/pods/com.hcnx.version)
[![License](https://img.shields.io/cocoapods/l/com.hcnx.version.svg?style=flat)](http://cocoapods.org/pods/com.hcnx.version)
[![Platform](https://img.shields.io/cocoapods/p/com.hcnx.version.svg?style=flat)](http://cocoapods.org/pods/com.hcnx.version)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

com.hcnx.version is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "com.hcnx.version"
```

## Author

Guillaume MARTINEZ, g.martinez@highconnexion.com

## License

com.hcnx.version is available under the MIT license. See the LICENSE file for more info.
